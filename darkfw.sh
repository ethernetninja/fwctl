#!/bin/bash
# darkfw - VPN Killswitch
# cal@ethernet.ninja

####
fslock=/tmp/darkfw.lock
tundev=tun0
green='\e[32m'
blue='\e[34m'
clear='\e[0m'
ColorGreen(){
	echo -ne $green$1$clear
}
ColorBlue(){
	echo -ne $blue$1$clear
}
if [ "$EUID" -ne 0 ]
  then echo "Must be run as root (or sudo), exiting."
  exit
fi
if [[ ! -f /usr/sbin/ufw ]] ; then
    echo 'UFW does not exist, exiting.'
    exit
fi
if [[ ! -f /usr/bin/curl ]] ; then
    echo 'cURL does not exist, exiting.'
    exit
fi
if [[ ! -f /usr/bin/whois ]] ; then
    echo 'whois does not exist, exiting.'
    exit
fi
####

lock()
{
	touch $fslock
	printf "Locking down firewall..\\n"
	ufw --force reset > /dev/null 2>&1 | grep ''
	ufw default deny incoming > /dev/null 2>&1 | grep ''
	ufw default deny outgoing > /dev/null 2>&1 | grep ''
	ufw allow out on $tundev from any to any > /dev/null 2>&1 | grep ''
	ufw enable > /dev/null 2>&1 | grep ''
	printf "Done\\n"
}

unlock()
{
	rm -rf $fslock
    printf "Unlocking firewall..\\n"
	ufw --force reset > /dev/null 2>&1 | grep ''
	ufw default deny incoming > /dev/null 2>&1 | grep ''
	ufw default allow outgoing > /dev/null 2>&1 | grep ''
	ufw enable > /dev/null 2>&1 | grep ''
	printf "Done\\n"
}

status()
{
	if [ -f "$fslock" ]; then
		printf "status: enabled (active)\\n"
	else 
		printf "status: disabled (inactive)\\n"
	fi

	if ping -q -c 1 -W 1 1.1.1.1 >/dev/null; then
  		echo -ne $(ColorGreen 'IPv4 is UP\\n')
		pubip=$(curl -s --max-time 2 'ipecho.net/plain')
		echo "Public IP: $pubip"
		iporg=$(whois $pubip | grep Organization)
		echo "Public IP $iporg"
		else
  		echo -ne $(ColorBlue 'IPv4 is DOWN\\n')
	fi

}

fwlock()
{
	lock
	echo -ne $(ColorGreen 'Locked\\n')
}

fwunlock()
{
	unlock
	echo -ne $(ColorGreen 'Unlocked\\n')
}

fwstatus()
{
	status
}

if [ "$1" == "lock" ]; then
fwlock
exit
fi

if [ "$1" == "unlock" ]; then
fwunlock
exit
fi

if [ "$1" == "status" ]; then
fwstatus
exit
fi

printf "Arguments: unlock, lock, status\\n"
printf "Connect to VPN, lock killswitch. Unlock killswitch after VPN disconnect.\\n"
exit