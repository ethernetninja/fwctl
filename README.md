## Getting started (Ubuntu)
Clone this repository and move darkfw.sh to /usr/local/bin for easy access. 

## Usage
darkfw is designed to act as a VPN killswitch by blocking all traffic NOT sent to your VPN, preventing IP leaks. By default darkfw assumes you are using OpenVPN with a tunnel device of tun0. You can edit the $tundev variable in the script to work with other setups. In order to use darkfw you will need to connect to your VPN first before activating the killswitch.

1. Connect to VPN
2. Run darkfw lock
3. Browse securely

When done, disconnect your VPN and then run 'darkfw unlock' to reset killswitch.

## Screenshot
![alt-text](doc/doc.png)

## Support
no
## Roadmap
darkfw is only meant for Ubuntu (using ufw) at this time